# Sun System Test #

Каждая планета(кроме центральной) вращается относительно другой.
```java
public Planet(float radius, float mass, Planet targetPlanet) {...}

Planet somePlanet = new Planet(20, 20, null); // null - нет планеты относительно которой будет вращаться

Planet otherPlanet = new Planet(20, 20, somePlanet); // будет вращаться относительно somePlanet
```
Скорость:  
Пусть она зависит от массы.
```java
speed = (float) Math.sqrt(target.getMass() / mass);
```
Вращение:
```java
position.x = (float) (target.getPosition().x + Math.sin(times * speed) * mRange);
position.y = (float) (target.getPosition().y + Math.cos(times * speed) * mRange);
```