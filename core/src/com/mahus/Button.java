package com.mahus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by mhs on 8/15/17.
 */
public class Button {
    private SpriteBatch batcher;
    private Rectangle bounds;
    private Texture texture;
    private String uID;

    private boolean pressed = false;

    public Button(float x, float y, float width, float height, String id) {
        batcher = new SpriteBatch();
        texture = new Texture("img/button.png");
        bounds = new Rectangle(x, y, width, height);
        uID = id;
    }

    public void loadTexture(String path) {
        texture = new Texture(path);

    }
    public String getID() {
        return uID;
    }

    public boolean isDown(float x, float y) {
        if (bounds.contains(x, y)) {
            pressed = true;
            return true;
        }
        return false;
    }
    public boolean isUp(float x, float y) {
        if (bounds.contains(x, y) && pressed) {
            pressed = false;
            Gdx.app.log("Button", "event");
            return true;
        }
        pressed = false;
        return false;
    }
    public void draw(Camera camera) {
        batcher.setProjectionMatrix(camera.combined);
        batcher.begin();
        if (pressed) {
            batcher.draw(texture, bounds.x, bounds.y, bounds.width-5, bounds.height-5);
        } else {
            batcher.draw(texture, bounds.x, bounds.y, bounds.width, bounds.height);
        }
        batcher.end();
    }
}
