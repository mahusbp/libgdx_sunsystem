package com.mahus;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

import java.util.ArrayList;

/**
 * Created by mhs on 8/15/17.
 */
public class EventListener implements InputProcessor {
    private ArrayList<Button> mButtons;
    public GameWorld mWorld;

    EventListener(GameWorld world) {
        mWorld = world;
        mButtons = mWorld.getButtons();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            for (Button but : mButtons) {
                but.isDown(screenX, screenY);
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            for (Button but : mButtons) {
                if (but.isUp(screenX, screenY) && but.getID().equals("pause")) {
                    if (mWorld.isPaused())
                        mWorld.pause(false);
                    else
                        mWorld.pause(true);
                }
            }
        }
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
