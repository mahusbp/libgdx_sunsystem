package com.mahus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by mhs on 8/15/17.
 */
public class GameRenderer {

    private OrthographicCamera camera;
    private GameWorld mWorld;

    public GameRenderer(GameWorld world) {
        mWorld = world;
        camera = new OrthographicCamera();
        camera.setToOrtho(true, 500, 500);
    }

    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        for (Planet planet : mWorld.getPlanets()) {
            planet.draw(camera);
        }
        for (Button button : mWorld.getButtons()) {
            button.draw(camera);
        }
    }
}
