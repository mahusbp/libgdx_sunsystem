package com.mahus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

/**
 * Created by mhs on 8/15/17.
 */

public class GameScreen implements Screen {

    private GameRenderer renderer;
    private GameWorld world;

    public GameScreen() {
        world = new GameWorld();
        renderer = new GameRenderer(world);
        Gdx.input.setInputProcessor(new EventListener(world));
    }

    @Override
    public void show() {
        Gdx.app.log("GameScreen", "show");
    }

    @Override
    public void render(float delta) {
        if (!world.isPaused()) {
            world.update(delta);
        }
        renderer.render();
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.log("GameScreen", "resize");
    }

    @Override
    public void pause() {
        world.pause(true);
    }

    @Override
    public void resume() {
        world.pause(false);
    }

    @Override
    public void hide() {
        Gdx.app.log("GameScreen", "hide");
    }

    @Override
    public void dispose() {
        Gdx.app.log("GameScreen", "dispose");
    }
}
