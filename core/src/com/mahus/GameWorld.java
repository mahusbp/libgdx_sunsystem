package com.mahus;


import com.badlogic.gdx.Gdx;

import java.util.ArrayList;

/**
 * Created by mhs on 8/15/17.
 */
public class GameWorld {

    private ArrayList<Planet> planets;
    private ArrayList<Button> buttons;
    private boolean paused = false;

    public GameWorld() {
        planets = new ArrayList<Planet>();
        planets.add(new Planet(20, 20, null));
        planets.get(0).setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        planets.get(0).setColor(255, 192, 32, 150);
        planets.add(new Planet(10, 10, planets.get(0)));
        planets.get(1).setColor(65, 255, 100, 150);
        planets.add(new Planet(15, 15, planets.get(0)));
        planets.get(2).setColor(65, 100, 255, 150);
        planets.add(new Planet(5, 5, planets.get(2)));

        buttons = new ArrayList<Button>();
        buttons.add(new Button(10, 10, 75, 40, "pause"));
        buttons.get(0).loadTexture("img/button.png");
    }

    public void update(float delta) {
        for (Planet planet : planets) {
            planet.update(delta);
        }
    }
    public void pause(boolean stop) {
        paused = stop;
    }
    public boolean isPaused() {
        return paused;
    }
    public ArrayList<Planet> getPlanets() {
        return planets;
    }
    public ArrayList<Button> getButtons() {
        return buttons;
    }
}
