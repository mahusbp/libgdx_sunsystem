package com.mahus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by mhs on 8/15/17.
 */
public class Planet {
    private ShapeRenderer renderer;
    private Vector2 position;
    private Vector2 velocity;

    private float mRadius = 0;
    private float mRange = 0;
    private float times = 0;
    private float mMass = 0;
    private float speed = 0;

    private Planet target;

    public Planet(float radius, float mass, Planet targetPlanet) {
        renderer = new ShapeRenderer();
        position = new Vector2();
        velocity = new Vector2();
        mRadius = radius;
        mMass = mass;
        if (targetPlanet == null) target = this;
        else target = targetPlanet;
        if (target != this) {
            mRange = (mass * target.getMass()) / 2;
            position.x = target.getPosition().x;
            position.y = target.getPosition().y + mRange;
            // pseudo orbital speed
            speed = (float) Math.sqrt(target.getMass() / mass);
            Gdx.app.log("spd", speed + "");
        }
    }

    public void update(float delta) {
        if (target != this) {
            times += delta;
            position.x = (float) (target.getPosition().x + Math.sin(times * speed) * mRange);
            position.y = (float) (target.getPosition().y + Math.cos(times * speed) * mRange);
        }
    }
    public void setColor(int r, int g, int b, int a) {
        renderer.setColor(r/255f, g/255f, b/255f, a/255f);
    }
    public void draw(Camera camera) {
        renderer.setProjectionMatrix(camera.combined);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.circle(position.x, position.y, mRadius);
        renderer.end();
    }

    public void setPosition(float x, float y) {
        position.x = x;
        position.y = y;
    }
    public Vector2 getPosition() {
        return position;
    }
    public float getMass() {
        return mMass;
    }
}
